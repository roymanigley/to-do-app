# ToDo Webapplication

## Requirements:

- maven (developed with 3.3.9)
- Java 8
- Nodejs (developed with 10.16.0)
- npm (developed with 6.9.0)
- Ancular CLI
```
Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.802.0
@angular-devkit/core         8.2.0
@angular-devkit/schematics   8.2.0
@schematics/angular          8.2.0
@schematics/update           0.802.0
rxjs                         6.4.0
```

## Build Commands

### Build client

- run Angular CLI buld command `ng build --base-href=/to-do-client/ --prod=true` in `to-do-client/`

### Build service

1. Copy from `to-do-client/dist/to-do-client` to `to-do-service/src/main/resources/statc/to-do-client`

2. run maven build command `mvn clean package` in `to-do-service/`

3. start server `java -jar target/to-do-XY.jar` in `to-do-service/`

### Hint:
> Disable the SpringSecurityFilter while developing, set env `export DISABLE_SECURITY_FILTER=true` then run `ng serve --open`

## Deployment Commands

### Deploy .jar to Heroku
install java plugin  
`heroku plugins:install java`  

create application
`heroku create --no-remote` in `to-do-service/`  

deploy application   
`heroku deploy:jar target/to-do-1.0.jar --app to-do-board`  in `to-do-service/`

see log entries  
`heroku logs --tail --app to-do-board`

### Heroku Database Setup
add addon  
`heroku addons:create heroku-postgresql --app to-do-board --version=9.5`  
config by sys var @see `ch.bytecrowd.todo.config.DataSourceConfiguration`:
```bash
export JDBC_DATABASE_URL=jdbc:h2:./database/MyDataBase
export JDBC_DATABASE_USERNAME=test
export JDBC_DATABASE_PASSWORD=test
export JDBC_DATABASE_DRIVER=org.h2.Driver
```

### Other commands
restart  
`heroku dyno:restart --app to-do-board`