export class File {

    id: number;
    name: string;
    mimeType: string;
}
