import { State } from './state';
import { Tag } from './tag';

export class ToDoView {

    id: number;
    viewName: string;
    states: State[] = [];
    exitState: State;
    tags: Tag[] = [];
}
