import { ToDo } from './to-do';

export class ToDoRequirement {

    id: number;
    text: string;
    toDo: ToDo;
    done: Boolean = false;
}
