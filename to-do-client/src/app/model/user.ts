export class User {

    id: number;
    username: string;
    displayName: string;
    password: string;
    imageMimeType: string;
}
