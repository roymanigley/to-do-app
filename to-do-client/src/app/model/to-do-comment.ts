import { ToDo } from './to-do';
import { User } from './user';

export class ToDoComment {

    id: number;
    text: string;
    time: Date;
    user: User;
    toDo: ToDo;
}
