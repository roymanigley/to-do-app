import { Tag } from './tag';
import { State } from './state';
import { User } from './user';

export class ToDo {

    id: number;
    dueDate: Date;
    title: string;
    description: string;
    state: State;
    tags: Array<Tag> = [];
    files: Array<File> = [];
    users: Array<User> = [];
    showDetails: boolean = false;
    availableStates: Array<State> = [];
    countToDoRequirementsAll: number;
    countToDoRequirementsDone: number;
}
