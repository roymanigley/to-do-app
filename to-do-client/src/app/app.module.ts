import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToDoListComponent } from './component/crud/to-do-list/to-do-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { MatTabsModule } from '@angular/material/tabs'; 
import {MatSelectModule} from '@angular/material/select'; 
import {DragDropModule} from '@angular/cdk/drag-drop'; 
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card'; 
import { OverlayModule } from '@angular/cdk/overlay';
import { DeleteDialog } from './dialog/delete/delete-dialog';
import { EditToDoDialog } from './dialog/edit/to-do/edit-to-do-dialog';
import { EditFileDialog } from './dialog/edit/file/edit-file-dialog';
import {MatSnackBarModule, MatAutocompleteModule} from '@angular/material/';
import { TagListComponent } from './component/crud/tag-list/tag-list.component';
import { EditTagDialog } from './dialog/edit/tag/edit-tag-dialog';
import {MatMenuModule} from '@angular/material/menu'; 
import {MatProgressBarModule} from '@angular/material/progress-bar'; 
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {MatDatepickerModule} from '@angular/material/datepicker'; 

import { EditStateDialog } from './dialog/edit/state/edit-state-dialog';
import {MatChipsModule} from '@angular/material/chips';
import { StateListComponent } from './component/crud/state-list/state-list.component';
import { FileListComponent } from './component/crud/file-list/file-list.component';
import { RelatedFilesDialog } from './dialog/files/related-files-dialog';
import { ToDoBoardComponent } from './component/to-do-board/to-do-board.component';
import { UserListComponent } from './component/crud/user-list/user-list.component';
import { EditUserDialog } from './dialog/edit/user/edit-user-dialog';
import { ToDoViewListComponent } from './component/crud/to-do-view-list/to-do-view-list.component';
import { EditToDoViewDialog } from './dialog/edit/to-do-view/edit-to-do-view-dialog';
import { JsessionHttpInterceptor } from './interceptor/jsession-http-interceptor';
import { AuthService } from './service/auth-service.service';
import { LoginComponent } from './security/login/login.component';
import { EditPasswordDialog } from './dialog/password/edit-password-dialog';
import { RelatedCommentsDialog } from './dialog/comments/related-comments-dialog';
import { RelatedRequirementsDialog } from './dialog/requirements/related-requirements-dialog';

@NgModule({
  declarations: [
    AppComponent,
    ToDoListComponent,
    DeleteDialog,
    EditPasswordDialog,
    EditToDoDialog,
    EditFileDialog,
    EditTagDialog,
    EditStateDialog,
    EditUserDialog,
    EditToDoViewDialog,
    RelatedFilesDialog,
    RelatedRequirementsDialog,
    RelatedCommentsDialog,
    TagListComponent,
    StateListComponent,
    FileListComponent,
    ToDoBoardComponent,
    UserListComponent,
    ToDoViewListComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    NoopAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    DragDropModule,
    HttpClientModule,
    MatTableModule,
    MatInputModule,
    OverlayModule,
    MatDialogModule,
    MatCardModule,
    MatSnackBarModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatChipsModule,
    MatProgressBarModule,
    MatMenuModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatSelectModule
  ],
  providers: [

    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JsessionHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DeleteDialog,
    EditToDoDialog,
    EditFileDialog,
    EditTagDialog,
    EditStateDialog,
    EditUserDialog,
    EditToDoViewDialog,
    RelatedFilesDialog,
    EditPasswordDialog,
    RelatedCommentsDialog,
    RelatedRequirementsDialog
  ]
})
export class AppModule { }
