import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditToDoViewDialog } from './edit-to-do-view-dialog';

describe('EditToDoViewDialog', () => {
  let component: EditToDoViewDialog;
  let fixture: ComponentFixture<EditToDoViewDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditToDoViewDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditToDoViewDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
