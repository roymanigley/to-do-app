import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToDoView } from 'src/app/model/to-do-view';
import { Tag } from 'src/app/model/tag';
import { State } from 'src/app/model/state';
import { TagService } from 'src/app/service/tag-service.service';
import { StateService } from 'src/app/service/state-service.service';

@Component({
    selector: 'edit-to-do-view-dialog',
    templateUrl: 'edit-to-do-view-dialog.html',
  })
  export class EditToDoViewDialog  implements OnInit{
  
    selectedTags: Tag[] = [];
    selectedStates: State[] = [];
    allTags: Tag[] = [];
    allStates: State[] = [];

    constructor(
      private tagService: TagService, 
      private stateService: StateService,
      public dialogRef: MatDialogRef<EditToDoViewDialog>,
      @Inject(MAT_DIALOG_DATA) public data: ToDoView) {}
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        this.dialogRef.close(true);
      }

    ngOnInit(): void {
      
      this.tagService.findAll().subscribe((allTags) => {
        this.allTags = allTags;
      });
      this.stateService.findAll().subscribe((allStates) => {
        this.allStates = allStates.sort((stateA, stateB) => {return stateA.sorter - stateB.sorter});
      });
    }

    stateComparer(o1: State, o2: State): boolean {
      // if possible compare by object's name property - and not by reference.
      return o1 && o2 ? o1.id === o2.id : o2 === o2;
    }

    tagComparer(o1: Tag, o2: Tag): boolean {
      // if possible compare by object's name property - and not by reference.
      return o1 && o2 ? o1.id === o2.id : o2 === o2;
    }
  
  }