import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatSnackBar, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { MomentDateAdapter } from '@angular/material-moment-adapter';

import { ToDo } from 'src/app/model/to-do';
import { TagService } from 'src/app/service/tag-service.service';
import { StateService } from 'src/app/service/state-service.service';
import { FileService } from 'src/app/service/file-service.service';
import { RelatedFilesDialog } from '../../files/related-files-dialog';
import { RelatedCommentsDialog } from '../../comments/related-comments-dialog';
import { UserService } from 'src/app/service/user-service.service';
import { User } from 'src/app/model/user';
import { FileListComponent } from 'src/app/component/crud/file-list/file-list.component';
import { ToDoBoardComponent } from 'src/app/component/to-do-board/to-do-board.component';
import { RelatedRequirementsDialog } from '../../requirements/related-requirements-dialog';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@Component({
    selector: 'edit-to-do-dialog',
    templateUrl: 'edit-to-do-dialog.html',
    providers: [FileService, FileListComponent, MatDialog, MatSnackBar,
      {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}]
})
export class EditToDoDialog {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredTags: Observable<string[]>;
  tags: any = [];
  allTags: any = [];
  allStates: any = [];
  availableStates: any = [];
  allUsers: any = [];

  @ViewChild('tagInput', {static: false}) tagInput: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<EditToDoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ToDo, private tagService: TagService, private stateService: StateService, private fileService: FileService, private userService: UserService, private snackBar: MatSnackBar, public filesDialog: MatDialog) {
        this.tags = data.tags || [];
        this.filteredTags = this.tagCtrl.valueChanges.pipe(
        startWith(null),
        map((tag: string | null) => tag ? this._filter(tag) : this.allTags.slice()));
        if (data.availableStates != null && data.availableStates.length > 0) {
          this.availableStates = data.availableStates;
        }
    }

  ngOnInit() {
    this.userService.findAll().subscribe((allUsers) => {
      this.allUsers = allUsers;
    });
    this.tagService.findAll().subscribe((allTags) => {
      this.allTags = allTags;
    });

    if (this.availableStates.length == 0) {
      this.stateService.findAll().subscribe((allStates) => {
        this.allStates = allStates.sort((stateA, stateB) => {return stateA.sorter - stateB.sorter});
      });
    } else {
      this.allStates = this.availableStates;
    }
    /*
    this.stateService.findAll().subscribe((allStates) => {
      this.allStates = allStates.sort((stateA, stateB) => {return stateA.sorter - stateB.sorter});
    });*/
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }
  
  onYesClick(): void {
    this.data.tags = this.tags;
    this.dialogRef.close(true);
  }

  openFilesDialog(): void {
    const dialogRef = this.filesDialog.open(RelatedFilesDialog, {
      width: '80%',
      data: this.data
    });
  }

  openCommentsDialog(): void {
    const dialogRef = this.filesDialog.open(RelatedCommentsDialog, {
      width: '80%',
      data: this.data
    });
  }

  openRequirementsDialog(): void {
    const dialogRef = this.filesDialog.open(RelatedRequirementsDialog, {
      width: '80%',
      data: this.data
    });
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.tags.push({
          id:null,
          tagName:value.trim()
      });
    }

    if (input) {
      input.value = '';
    }

    this.tagCtrl.setValue(null);
  }

  removeTag(tag, indx): void {
    this.tags.splice(indx, 1);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.value);
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }

  private _filter(value: String): string[] {
    return this.allTags.filter(tag => {
      if (value.constructor.name == "String") {
        return tag.tagName.toLowerCase().indexOf(value.toLowerCase()) >= 0;
      } else {
        false;
      }
    });
  }

  public uploadFile(files) {
    
    if (this.data.id) {
      this.fileService.uploadFiles(files, this.data)
        .subscribe((response) => {
          this.snackBar.open("File saved", null, {
            duration: 2000,
          });
        },
          (error) => {
            this.snackBar.open("Failed to save file", null, {
              duration: 5000,
            });
          }
        );
    } else {
      this.snackBar.open("ToDo has to be saved before adding a file", null, {
        duration: 5000,
      });
    }
  }
  
  userComparer(o1: User, o2: User): boolean {
    // if possible compare by object's name property - and not by reference.
    return o1 && o2 ? o1.id === o2.id : o2 === o2;
  }
}