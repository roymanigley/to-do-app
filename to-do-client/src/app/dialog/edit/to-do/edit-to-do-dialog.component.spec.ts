import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditToDoDialog } from './edit-to-do-dialog';

describe('EditToDoDialog', () => {
  let component: EditToDoDialog;
  let fixture: ComponentFixture<EditToDoDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditToDoDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditToDoDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
