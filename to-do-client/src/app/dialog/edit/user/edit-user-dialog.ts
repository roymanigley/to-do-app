import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user-service.service';
import { MatSnackBar } from '@angular/material';
import { ImageUtil } from 'src/app/util/ImageUtil';

@Component({
    selector: 'edit-user-dialog',
    templateUrl: 'edit-user-dialog.html',
  })
  export class EditUserDialog {
  
    constructor(
      private userService: UserService,
      private snackBar: MatSnackBar,
      public dialogRef: MatDialogRef<EditUserDialog>,
      @Inject(MAT_DIALOG_DATA) public data: User) {}
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        this.dialogRef.close(true);
      }
  
      public uploadFile(files) {
        
        if (this.data.id) {
          this.userService.uploadFiles(files, this.data)
            .subscribe((response) => {
              this.snackBar.open("Image saved", null, {
                duration: 2000,
              });
            },
              (error) => {
                this.snackBar.open("Failed to save image", null, {
                  duration: 5000,
                });
              }
            );
        } else {
          this.snackBar.open("User has to be saved before adding a file", null, {
            duration: 5000,
          });
        }
      }



  getUserImageURL(user: User): String {
    
    return ImageUtil.getUserImageURL(user);
  }
}