import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserDialog } from './edit-user-dialog';

describe('EditUserDialog', () => {
  let component: EditUserDialog;
  let fixture: ComponentFixture<EditUserDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
