import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { State } from 'src/app/model/state';

@Component({
    selector: 'edit-state-dialog',
    templateUrl: 'edit-state-dialog.html',
  })
  export class EditStateDialog {
  
    constructor(
      public dialogRef: MatDialogRef<EditStateDialog>,
      @Inject(MAT_DIALOG_DATA) public data: State) {}
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        this.dialogRef.close(true);
      }
  
  }