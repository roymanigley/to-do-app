import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStateDialog } from './edit-state-dialog';

describe('EditStateDialog', () => {
  let component: EditStateDialog;
  let fixture: ComponentFixture<EditStateDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditStateDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStateDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
