import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFileDialog } from './edit-file-dialog';

describe('EditFileDialog', () => {
  let component: EditFileDialog;
  let fixture: ComponentFixture<EditFileDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFileDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFileDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
