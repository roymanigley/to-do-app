import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { File } from 'src/app/model/file';

@Component({
    selector: 'edit-file-dialog',
    templateUrl: 'edit-file-dialog.html',
  })
  export class EditFileDialog {
  
    constructor(
      public dialogRef: MatDialogRef<EditFileDialog>,
      @Inject(MAT_DIALOG_DATA) public data: File) {}
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        this.dialogRef.close(true);
      }
  
  }