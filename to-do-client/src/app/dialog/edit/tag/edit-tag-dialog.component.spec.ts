import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTagDialog } from './edit-tag-dialog';

describe('EditTagDialog', () => {
  let component: EditTagDialog;
  let fixture: ComponentFixture<EditTagDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTagDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTagDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
