import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tag } from 'src/app/model/tag';

@Component({
    selector: 'edit-tag-dialog',
    templateUrl: 'edit-tag-dialog.html',
  })
  export class EditTagDialog {
  
    constructor(
      public dialogRef: MatDialogRef<EditTagDialog>,
      @Inject(MAT_DIALOG_DATA) public data: Tag) {}
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        this.dialogRef.close(true);
      }
  
  }