import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { ToDo } from 'src/app/model/to-do';
import { AuthService } from 'src/app/service/auth-service.service';
import { ToDoRequirementService } from 'src/app/service/to-do-requirement-service.service';
import { ToDoRequirement } from 'src/app/model/to-do-requirement';
import { ToDoComment } from 'src/app/model/to-do-comment';

@Component({
    selector: 'related-requirements-dialog',
    templateUrl: 'related-requirements-dialog.html',
    providers: [ToDoRequirementService, MatDialog, MatSnackBar]
  })
  export class RelatedRequirementsDialog {
    
    displayedColumns: string[] = ['text', 'done', 'buttons'];
    dataSource = null;
    selectedRequirement: ToDoRequirement;


    constructor(
      public dialogRef: MatDialogRef<RelatedRequirementsDialog>,
      @Inject(MAT_DIALOG_DATA) public data: ToDo, private snackBar: MatSnackBar, private toDoRequirementService: ToDoRequirementService, private authService: AuthService) {

        this.clearSelectedAndReload();
      }
  
      onCloseClick(): void {
        this.dialogRef.close(false);
      }

      public saveSelected() {
        
        this.selectedRequirement.toDo = this.data;

        if (this.data.id) {
          this.toDoRequirementService.save(this.selectedRequirement)
            .subscribe((response) => {
              this.snackBar.open("Requirement saved", null, {
                duration: 2000,
              });
              this.clearSelectedAndReload();
            },
              (error) => {
                this.snackBar.open("Failed to save requirement", null, {
                  duration: 5000,
                });
              }
            );
        } else {
          this.snackBar.open("ToDo has to be saved before adding a requirement", null, {
            duration: 5000,
          });
        }
      }

      public checkRequirement(requirement: ToDoRequirement) {
        
        requirement.done = !requirement.done
        this.saveRequirement(requirement);
      }

      public deleteRequirement(requirement: ToDoRequirement) {
        
        this.toDoRequirementService.delete(requirement).subscribe((response) => {
          this.snackBar.open("Requirement deleted", null, {
            duration: 2000,
          });
          this.loadRequirements();
        },
          (error) => {
            this.snackBar.open("Failed to delete requirement", null, {
              duration: 5000,
            });
          }
        );
      }

      saveRequirement(requirement: ToDoRequirement) {
        
        if (this.data.id) {
          this.toDoRequirementService.save(requirement)
            .subscribe((response) => {
              this.snackBar.open("Requirement saved", null, {
                duration: 2000,
              });
              this.clearSelectedAndReload();
            },
              (error) => {
                this.snackBar.open("Failed to save requirement", null, {
                  duration: 5000,
                });
              }
            );
        } else {
          this.snackBar.open("ToDo has to be saved before adding a requirement", null, {
            duration: 5000,
          });
        }
      }

      private loadRequirements(): void {
        this.toDoRequirementService.findByToDo(this.data).subscribe(files => {
          this.dataSource = new MatTableDataSource(files);
        });
      }

      public setSelected(requirement: ToDoRequirement): void {
        
        this.selectedRequirement = requirement;
      }

      public clearSelectedAndReload(): void {
        this.loadRequirements()
        this.selectedRequirement = new ToDoRequirement();
      }
  }