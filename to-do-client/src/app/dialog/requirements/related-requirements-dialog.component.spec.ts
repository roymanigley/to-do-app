import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RelatedCommentsDialog } from './related-comments-dialog';


describe('RelatedFilesDialog', () => {
  let component: RelatedCommentsDialog;
  let fixture: ComponentFixture<RelatedCommentsDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedCommentsDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedCommentsDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
