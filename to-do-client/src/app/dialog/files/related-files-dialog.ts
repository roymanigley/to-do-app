import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { File } from 'src/app/model/file';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { FileService } from 'src/app/service/file-service.service';
import { ToDo } from 'src/app/model/to-do';
import { DeleteDialog } from '../delete/delete-dialog';
import { FileListComponent } from 'src/app/component/crud/file-list/file-list.component';
import { EditFileDialog } from '../edit/file/edit-file-dialog';

@Component({
    selector: 'related-files-dialog',
    templateUrl: 'related-files-dialog.html',
    providers: [FileService, FileListComponent, MatDialog, MatSnackBar]
  })
  export class RelatedFilesDialog {
    
    displayedColumns: string[] = ['id', 'name', 'mimeType', 'buttons'];
    dataSource = null;

    constructor(
      public dialogRef: MatDialogRef<RelatedFilesDialog>,
      @Inject(MAT_DIALOG_DATA) public data: ToDo, private snackBar: MatSnackBar, private fileService: FileService, public dialog: MatDialog, private fileListComponent: FileListComponent) {

        fileService.findByToDo(data).subscribe(files => {
          this.dataSource = new MatTableDataSource(files);
        });
      }
  
      onCloseClick(): void {
        this.dialogRef.close(false);
      }
      public uploadFile(files) {
        
        if (this.data.id) {
          this.fileService.uploadFiles(files, this.data)
            .subscribe((response) => {
              this.snackBar.open("File saved", null, {
                duration: 2000,
              });


              this.fileService.findByToDo(this.data).subscribe(files => {
                this.dataSource = new MatTableDataSource(files);
              });
            },
              (error) => {
                this.snackBar.open("Failed to save file", null, {
                  duration: 5000,
                });
              }
            );
        } else {
          this.snackBar.open("ToDo has to be saved before adding a file", null, {
            duration: 5000,
          });
        }
      }

      openEditDialog(file: File): void {
        const dialogRef = this.dialog.open(EditFileDialog, {
          width: '80%',
          data: file
        });
    
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          if (result) {
            this.fileListComponent.save(file);
          }
        });
      }

      downloadFile(file: File): void {
        
        this.fileService.downloadFile(file);
      }
  
      openDeleteDialog(file: File): void {
    
        const dialogRef = this.dialog.open(DeleteDialog, {
          data: file
        });

    
        dialogRef.afterClosed().subscribe(result => {
    
          
          console.log('The dialog was closed: ' + result);
          if (result) {
            this.fileService.delete(file).subscribe(
              (val) => {
                this.snackBar.open("Record deleted", null, {
                  duration: 2000,
                });
                console.log(val);
              },
              response => {
        
                this.snackBar.open("Failed to deleted record", null, {
                  duration: 5000,
                });
              },
              () => {
                    this.fileService.findByToDo(this.data).subscribe(files => {
                    this.dataSource = new MatTableDataSource(files);
                  });
              }
            );
          }
          
        });
      }
  }