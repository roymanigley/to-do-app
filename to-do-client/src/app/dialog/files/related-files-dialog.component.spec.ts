import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedFilesDialog } from './related-files-dialog';

describe('RelatedFilesDialog', () => {
  let component: RelatedFilesDialog;
  let fixture: ComponentFixture<RelatedFilesDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedFilesDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedFilesDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
