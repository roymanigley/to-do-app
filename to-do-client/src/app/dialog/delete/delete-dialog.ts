import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToDo } from 'src/app/model/to-do';

@Component({
    selector: 'delete-dialog',
    templateUrl: 'delete-dialog.html',
  })
  export class DeleteDialog {
  
    constructor(
      public dialogRef: MatDialogRef<DeleteDialog>,
      @Inject(MAT_DIALOG_DATA) public data: ToDo) {}
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        this.dialogRef.close(true);
      }
  
  }