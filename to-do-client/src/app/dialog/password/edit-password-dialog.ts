import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user';
import { ImageUtil } from 'src/app/util/ImageUtil';

@Component({
    selector: 'edit-password-dialog',
    templateUrl: 'edit-password-dialog.html',
  })
  export class EditPasswordDialog {

    password: string;
    passwordRepeat: string;
  
    constructor(
      public dialogRef: MatDialogRef<EditPasswordDialog>,
      @Inject(MAT_DIALOG_DATA) public data: User) {

      }
  
      onNoClick(): void {
        this.dialogRef.close(false);
      }
      onYesClick(): void {
        if (this.password == this.passwordRepeat) {
          this.data.password = this.password;
          this.dialogRef.close(true);
        } else {
          this.dialogRef.close(false);
        }
      }
  
      getUserImageURL(user: User): String {
    
        return ImageUtil.getUserImageURL(user);
      }
  }