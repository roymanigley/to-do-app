import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { ToDo } from 'src/app/model/to-do';
import { ToDoCommentService } from 'src/app/service/to-do-comment-service.service';
import { ToDoComment } from 'src/app/model/to-do-comment';
import { AuthService } from 'src/app/service/auth-service.service';
import { ImageUtil } from 'src/app/util/ImageUtil';
import { User } from 'src/app/model/user';

@Component({
    selector: 'related-comments-dialog',
    templateUrl: 'related-comments-dialog.html',
    providers: [ToDoCommentService, MatDialog, MatSnackBar]
  })
  export class RelatedCommentsDialog {
    
    displayedColumns: string[] = ['time', 'text', 'user'];
    dataSource = null;

    commentText: string;


    constructor(
      public dialogRef: MatDialogRef<RelatedCommentsDialog>,
      @Inject(MAT_DIALOG_DATA) public data: ToDo, private snackBar: MatSnackBar, private toDoCommentService: ToDoCommentService, private authService: AuthService) {

        this.loadComments();
      }
  
      onCloseClick(): void {
        this.dialogRef.close(false);
      }
      public addComment() {
        
        let comment: ToDoComment = new ToDoComment();
        comment.text = this.commentText;
        comment.user = this.authService.getCurrentUser();
        comment.toDo = this.data;
        
        if (this.data.id) {
          this.toDoCommentService.save(comment)
            .subscribe((response) => {
              this.snackBar.open("Comment saved", null, {
                duration: 2000,
              });
              this.loadComments();
              this.commentText = null;
            },
              (error) => {
                this.snackBar.open("Failed to save comment", null, {
                  duration: 5000,
                });
              }
            );
        } else {
          this.snackBar.open("ToDo has to be saved before adding a comment", null, {
            duration: 5000,
          });
        }
      }

      private loadComments(): void {
        this.toDoCommentService.findByToDo(this.data).subscribe(files => {
          this.dataSource = new MatTableDataSource(files);
        });
      }

      getUserImageURL(user: User) {
        return ImageUtil.getUserImageURL(user);
      }
  }