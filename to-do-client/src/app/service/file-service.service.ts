import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { File } from '../model/file';
import { Observable } from 'rxjs';
import { ToDo } from '../model/to-do';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private baseUrl = environment.restApplicationBaseUrl + 'File/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<File[]> {
    return this.http.get<File[]>(this.baseUrl);
  }

  public findByToDo(toDo: ToDo): Observable<File[]> {
    return this.http.get<File[]>(this.baseUrl + 'ToDo/' + toDo.id);
  }

  public save(file: File) {
    return this.http.post<File>(this.baseUrl, file);
  }

  public delete(file: File) {
    return this.http.delete(this.baseUrl + file.id);
  }

  public uploadFiles(files, toDo: ToDo) {
    console.log('files', files)
        var formData = new FormData();

    for(let i =0; i < files.length; i++){
      formData.append("file", files[i], files[i]['name']);
    }

    return this.http.post(this.baseUrl + 'ToDo/' + toDo.id, formData);
  }

  public downloadFile(file: File): void {

    window.location.href=this.baseUrl + file.id;
  }
}
