import { TestBed } from '@angular/core/testing';

import { ToDoRequirementServiceService } from './to-do-requirement-service.service';

describe('ToDoRequirementServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToDoRequirementServiceService = TestBed.get(ToDoRequirementServiceService);
    expect(service).toBeTruthy();
  });
});
