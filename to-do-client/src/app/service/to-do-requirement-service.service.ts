import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToDoRequirement } from '../model/to-do-requirement';
import { Observable } from 'rxjs';
import { ToDo } from '../model/to-do';

@Injectable({
  providedIn: 'root'
})
export class ToDoRequirementService {

  private baseUrl = environment.restApplicationBaseUrl + 'ToDoRequirement/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<ToDoRequirement[]> {
    return this.http.get<ToDoRequirement[]>(this.baseUrl);
  }

  public findByToDo(toDo: ToDo): Observable<ToDoRequirement[]> {
    return this.http.get<ToDoRequirement[]>(this.baseUrl + "ToDo/"+ toDo.id);
  }

  public save(toDoRequirement: ToDoRequirement) {
    return this.http.post<ToDoRequirement>(this.baseUrl, toDoRequirement);
  }

  public delete(toDoRequirement: ToDoRequirement) {
    return this.http.delete(this.baseUrl + toDoRequirement.id);
  }
}
