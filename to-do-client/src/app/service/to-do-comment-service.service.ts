import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToDoComment } from '../model/to-do-comment';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ToDo } from '../model/to-do';

@Injectable({
  providedIn: 'root'
})
export class ToDoCommentService {

  private baseUrl = environment.restApplicationBaseUrl + 'ToDoComment/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<ToDoComment[]> {
    return this.http.get<ToDoComment[]>(this.baseUrl);
  }

  public findByToDo(toDo: ToDo): Observable<ToDoComment[]> {
    return this.http.get<ToDoComment[]>(this.baseUrl + "ToDo/"+ toDo.id);
  }

  public save(toDoComment: ToDoComment) {
    return this.http.post<ToDoComment>(this.baseUrl, toDoComment);
  }

  public delete(toDoComment: ToDoComment) {
    return this.http.delete(this.baseUrl + toDoComment.id);
  }
}
