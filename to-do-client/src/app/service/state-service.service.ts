import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { State } from '../model/state';
import { Observable } from 'rxjs';
import { ToDo } from '../model/to-do';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private baseUrl = environment.restApplicationBaseUrl + 'State/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<State[]> {
    return this.http.get<State[]>(this.baseUrl);
  }

  public save(state: State) {
    return this.http.post<State>(this.baseUrl, state);
  }

  public delete(state: State) {
    return this.http.delete(this.baseUrl + state.id);
  }
}
