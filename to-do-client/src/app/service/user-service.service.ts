import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.restApplicationBaseUrl + 'User/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  public findById(id: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + id);
  }

  public save(user: User) {
    
    let urlParam = "?password=" + encodeURI(user.password);
    return this.http.post<User>(this.baseUrl + urlParam, user);
  }

  public updatePassword(user: User) {
    
    return this.http.post<User>(this.baseUrl + user.id, user.password);
  }

  public uploadFiles(files, user: User) {
    console.log('files', files)
    var formData = new FormData();
    formData.append("file", files[0], files[0]['name']);

    return this.http.post(this.baseUrl + 'Image/' + user.id, formData);
  }

  public delete(user: User) {
    return this.http.delete(this.baseUrl + user.id);
  }
}
