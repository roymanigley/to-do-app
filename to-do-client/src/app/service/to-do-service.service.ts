import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToDo } from '../model/to-do';
import { Observable } from 'rxjs';
import { ToDoView } from '../model/to-do-view';
import { environment } from 'src/environments/environment';
import { State } from '../model/state';
import { staticViewQueryIds } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {
  
  private baseUrl = environment.restApplicationBaseUrl + 'ToDo/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<ToDo[]> {
    return this.http.get<ToDo[]>(this.baseUrl);
  }

  public findAllByStates(states: State[]): Observable<ToDo[]> {
    let sateIds = states
      .filter(state => state.id != null)
      .map(state => state.id);

    return this.http.post<ToDo[]>(this.baseUrl + "State", sateIds);
  }

  public findAllByToDoView(toDoView: ToDoView): Observable<ToDo[]> {
    return this.http.get<ToDo[]>(this.baseUrl + toDoView.id);
  }

  public save(toDo: ToDo) {
    return this.http.post<ToDo>(this.baseUrl, toDo);
  }

  public delete(toDo: ToDo) {
    return this.http.delete(this.baseUrl + toDo.id);
  }
}
