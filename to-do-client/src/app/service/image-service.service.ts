import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageUtil } from '../util/ImageUtil';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: HttpClient, private domSanitizer: DomSanitizer) { }

  public loadImage(user: User): Observable<Blob> {
    let url = ImageUtil.getUserImageURL(user);
    return this.http.get(url, {
      responseType: "blob"
    });
  }
}
