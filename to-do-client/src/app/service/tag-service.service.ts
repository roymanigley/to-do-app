import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tag } from '../model/tag';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TagService {
  
  private baseUrl = environment.restApplicationBaseUrl + 'Tag/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.baseUrl);
  }

  public save(tag: Tag) {
    return this.http.post<Tag>(this.baseUrl, tag);
  }

  public delete(tag: Tag) {
    return this.http.delete(this.baseUrl + tag.id);
  }
}
