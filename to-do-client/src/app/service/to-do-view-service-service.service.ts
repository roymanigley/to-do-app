import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToDoView } from '../model/to-do-view';
import { Observable } from 'rxjs';
import { ToDo } from '../model/to-do';
import { environment } from 'src/environments/environment';
import { State } from '../model/state';

@Injectable({
  providedIn: 'root'
})
export class ToDoViewService {

  private baseUrl = environment.restApplicationBaseUrl + 'ToDoView/';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<ToDoView[]> {
    return this.http.get<ToDoView[]>(this.baseUrl);
  }

  public findByToDoView(toDoView: ToDoView): Observable<ToDoView> {
    return this.http.get<ToDoView>(this.baseUrl + toDoView.id);
  }

  public save(toDoView: ToDoView) {
    return this.http.post<ToDoView>(this.baseUrl, toDoView);
  }

  public delete(toDoView: ToDoView) {
    return this.http.delete(this.baseUrl + toDoView.id);
  }
}
