import { TestBed } from '@angular/core/testing';

import { ToDoCommentService } from './to-do-comment-service.service';

describe('ToDoCommentServiceServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToDoCommentService = TestBed.get(ToDoCommentService);
    expect(service).toBeTruthy();
  });
});
