import { TestBed } from '@angular/core/testing';

import { ToDoViewService } from './to-do-view-service-service.service';

describe('ToDoViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToDoViewService = TestBed.get(ToDoViewService);
    expect(service).toBeTruthy();
  });
});
