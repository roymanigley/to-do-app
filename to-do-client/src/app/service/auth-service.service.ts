import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserService } from './user-service.service';
import { User } from '../model/user';

@Injectable()
export class AuthService {
  
    private JSESSIONID_KEY = "JSESSIONID";
    private USER_ID_KEY = "USER_ID";
    private currentUser: User;
    private authApplicationLoginUrl = environment.authApplicationLoginUrl;
  
    constructor(private http: HttpClient, private userService: UserService) {
        
    }

    public login(username: String, password: String, onSuccess: Function, onError: Function): void {
                
        let headers = new HttpHeaders();
        headers = headers.append("Authorization", "Basic " + btoa(username + ":" + password));
        headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
        
        this.http.get<Map<String, String>>(this.authApplicationLoginUrl + "?username=" + username + "&password=" + password, { headers }).subscribe((map: Map<String, String>) => {

            sessionStorage.setItem(this.JSESSIONID_KEY, map["JSESSIONID"]);
            sessionStorage.setItem(this.USER_ID_KEY, map["USER_ID"]);
            this.userService.findById(parseInt(sessionStorage.getItem(this.USER_ID_KEY))).subscribe((user) => {
                this.currentUser = user;
                onSuccess.call(this);
            }, () => {
                onError.call(this);
            });
        }, () => {
            onError.call(this);
        });
    }

    logout() {
        this.currentUser = null; 
        sessionStorage.removeItem(this.JSESSIONID_KEY);
        sessionStorage.removeItem(this.USER_ID_KEY);
        return this.http.get(this.authApplicationLoginUrl + "logout");
    }
  
    isUserLoggedIn(): Boolean {

        return sessionStorage.getItem(this.USER_ID_KEY) && sessionStorage.getItem(this.JSESSIONID_KEY) != null;
    }
    
    public getJSessionId(): String {
        
        return sessionStorage.getItem(this.JSESSIONID_KEY);
    }
  
    public getCurrentUser(): User {
        /*
        console.log("getCurrentUser: ");
        console.log("USER_ID_KEY: " + sessionStorage.getItem(this.USER_ID_KEY));
        console.log("JSESSIONID_KEY: " + sessionStorage.getItem(this.JSESSIONID_KEY));
        */
        if (sessionStorage.getItem(this.USER_ID_KEY) != null && sessionStorage.getItem(this.JSESSIONID_KEY) != null && this.currentUser == null) {
            this.userService.findById(parseInt(sessionStorage.getItem(this.USER_ID_KEY))).subscribe((user) => {
                    this.currentUser = user;
            }, () => {
                this.logout();
            });
        }
        return this.currentUser;
    }
}