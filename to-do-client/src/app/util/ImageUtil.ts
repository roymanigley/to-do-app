import { environment } from 'src/environments/environment.prod';
import { User } from '../model/user';

export class ImageUtil {

    private constructor() { }

    public static getUserImageURL(user: User): string {
    
        return environment.restApplicationBaseUrl + "User/Image/" + user.id; 
      
    }
}