import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../../../model/user';
import { UserService } from '../../../service/user-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DeleteDialog } from '../../../dialog/delete/delete-dialog';
import { MatSnackBar } from '@angular/material';
import { EditUserDialog } from '../../../dialog/edit/user/edit-user-dialog';
import { ImageUtil } from 'src/app/util/ImageUtil';
import { EditPasswordDialog } from 'src/app/dialog/password/edit-password-dialog';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];
  displayedColumns: string[] = ['id', 'image', 'username', 'displayname', 'buttons'];
  dataSource = null;

  constructor(private userService: UserService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.ngFetchData();
  }

  ngFetchData() {

    this.userService.findAll().subscribe(data => {
      this.users = data;
      this.dataSource = new MatTableDataSource(this.users);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(user: User) {
    this.userService.delete(user).subscribe(
      (val) => {
        this.snackBar.open("Record deleted", null, {
          duration: 2000,
        });
      },
      response => {

        this.snackBar.open("Failed to deleted record", null, {
          duration: 5000,
        });
      },
      () => {
          this.ngFetchData();
      }
    );
  }

  save(user: User) {
    this.userService.save(user).subscribe(
      (val) => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
      response => {
        this.snackBar.open("Failed to save record", null, {
          duration: 5000,
        });
      },
      () => {
          console.log("The SAVE observable is now completed.");
          this.ngFetchData();
      }
    );
  }

  updatePassword(user: User) {
    
    this.userService.updatePassword(user).subscribe(
      (val) => {
        this.snackBar.open("Password changed", null, {
          duration: 2000,
        });
      },
      response => {
        this.snackBar.open("Failed to change password", null, {
          duration: 5000,
        });
      }
    );
  }

  openCreateDialog(): void {
    this.openEditDialog(new User());
  }

  openEditDialog(user: User): void {
    const dialogRef = this.dialog.open(EditUserDialog, {
      width: '80%',
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.save(user);
      }
    });
  }

  openPasswordDialog(user: User): void {
    const dialogRef = this.dialog.open(EditPasswordDialog, {
      width: '80%',
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.updatePassword(user);
      } else {
        this.snackBar.open("Password unchanged", null, {
          duration: 5000,
        });
      }
    });
  }

  openDeleteDialog(user: User): void {
    
    const dialogRef = this.dialog.open(DeleteDialog, {
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.delete(user);
      }
    });
  }

  getUserImageURL(user: User): String {
    
    return ImageUtil.getUserImageURL(user);
  }
}
