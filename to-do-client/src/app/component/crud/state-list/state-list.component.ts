import { Component, OnInit, Inject } from '@angular/core';
import { State } from '../../../model/state';
import { StateService } from '../../../service/state-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DeleteDialog } from '../../../dialog/delete/delete-dialog';
import { MatSnackBar } from '@angular/material';
import { EditStateDialog } from '../../../dialog/edit/state/edit-state-dialog';

@Component({
  selector: 'app-state-list',
  templateUrl: './state-list.component.html',
  styleUrls: ['./state-list.component.css']
})
export class StateListComponent implements OnInit {

  states: State[];
  displayedColumns: string[] = ['id', 'name', 'sorter', 'buttons'];
  dataSource = null;

  constructor(private stateService: StateService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.ngFetchData();
  }

  ngFetchData() {

    this.stateService.findAll().subscribe(data => {
      this.states = data.sort((stateA, stateB) => {return stateA.sorter - stateB.sorter});
      this.dataSource = new MatTableDataSource(this.states);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(state: State) {
    this.stateService.delete(state).subscribe(
      (val) => {
        this.snackBar.open("Record deleted", null, {
          duration: 2000,
        });
      },
      response => {

        this.snackBar.open("Failed to deleted record", null, {
          duration: 5000,
        });
      },
      () => {
          this.ngFetchData();
      }
    );
  }

  save(state: State) {
    this.stateService.save(state).subscribe(
      (val) => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
      response => {
        this.snackBar.open("Failed to save record", null, {
          duration: 5000,
        });
      },
      () => {
          console.log("The SAVE observable is now completed.");
          this.ngFetchData();
      }
    );
  }


  openCreateDialog(): void {
    this.openEditDialog(new State());
  }

  openEditDialog(state: State): void {
    const dialogRef = this.dialog.open(EditStateDialog, {
      width: '80%',
      data: state
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.save(state);
      }
    });
  }

  openDeleteDialog(state: State): void {
    
    const dialogRef = this.dialog.open(DeleteDialog, {
      data: state
    });

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed: ' + result);
      if (result) {
        this.delete(state);
      }
      
    });
  }

}
