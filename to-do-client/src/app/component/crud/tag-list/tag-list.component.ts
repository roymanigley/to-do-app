import { Component, OnInit, Inject } from '@angular/core';
import { Tag } from '../../../model/tag';
import { TagService } from '../../../service/tag-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DeleteDialog } from '../../../dialog/delete/delete-dialog';
import { MatSnackBar } from '@angular/material';
import { EditTagDialog } from '../../../dialog/edit/tag/edit-tag-dialog';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {

  tags: Tag[];
  displayedColumns: string[] = ['id', 'tagName', 'buttons'];
  dataSource = null;

  constructor(private tagService: TagService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.ngFetchData();
  }

  ngFetchData() {

    this.tagService.findAll().subscribe(data => {
      this.tags = data;
      this.dataSource = new MatTableDataSource(this.tags);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(tag: Tag) {
    this.tagService.delete(tag).subscribe(
      (val) => {
        this.snackBar.open("Record deleted", null, {
          duration: 2000,
        });
      },
      response => {

        this.snackBar.open("Failed to deleted record", null, {
          duration: 5000,
        });
      },
      () => {
          this.ngFetchData();
      }
    );
  }

  save(tag: Tag) {
    this.tagService.save(tag).subscribe(
      (val) => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
      response => {
        this.snackBar.open("Failed to save record", null, {
          duration: 5000,
        });
      },
      () => {
          console.log("The SAVE observable is now completed.");
          this.ngFetchData();
      }
    );
  }


  openCreateDialog(): void {
    this.openEditDialog(new Tag());
  }

  openEditDialog(tag: Tag): void {
    const dialogRef = this.dialog.open(EditTagDialog, {
      width: '80%',
      data: tag
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.save(tag);
      }
    });
  }

  openDeleteDialog(tag: Tag): void {
    
    const dialogRef = this.dialog.open(DeleteDialog, {
      data: tag
    });

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed: ' + result);
      if (result) {
        this.delete(tag);
      }
      
    });
  }

}
