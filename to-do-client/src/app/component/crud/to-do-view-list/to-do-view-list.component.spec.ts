import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoViewListComponent } from './to-do-view-list.component';

describe('ToDoViewListComponent', () => {
  let component: ToDoViewListComponent;
  let fixture: ComponentFixture<ToDoViewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToDoViewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoViewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
