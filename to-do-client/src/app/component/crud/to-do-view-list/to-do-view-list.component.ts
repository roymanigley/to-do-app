import { Component, OnInit, Inject } from '@angular/core';
import { ToDoView } from '../../../model/to-do-view';
import { ToDoViewService } from '../../../service/to-do-view-service-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DeleteDialog } from '../../../dialog/delete/delete-dialog';
import { MatSnackBar } from '@angular/material';
import { EditToDoViewDialog } from '../../../dialog/edit/to-do-view/edit-to-do-view-dialog';

@Component({
  selector: 'app-to-do-view-list',
  templateUrl: './to-do-view-list.component.html',
  styleUrls: ['./to-do-view-list.component.css']
})
export class ToDoViewListComponent implements OnInit {

  toDoViews: ToDoView[];
  displayedColumns: string[] = ['id', 'viewName', 'buttons'];
  dataSource = null;

  constructor(private toDoViewService: ToDoViewService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.ngFetchData();
  }

  ngFetchData() {

    this.toDoViewService.findAll().subscribe(data => {
      this.toDoViews = data;
      this.dataSource = new MatTableDataSource(this.toDoViews);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(toDoView: ToDoView) {
    this.toDoViewService.delete(toDoView).subscribe(
      (val) => {
        this.snackBar.open("Record deleted", null, {
          duration: 2000,
        });
      },
      response => {

        this.snackBar.open("Failed to deleted record", null, {
          duration: 5000,
        });
      },
      () => {
          this.ngFetchData();
      }
    );
  }

  save(toDoView: ToDoView) {
    this.toDoViewService.save(toDoView).subscribe(
      (val) => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
      response => {
        this.snackBar.open("Failed to save record", null, {
          duration: 5000,
        });
      },
      () => {
          console.log("The SAVE observable is now completed.");
          this.ngFetchData();
      }
    );
  }


  openCreateDialog(): void {
    this.openEditDialog(new ToDoView());
  }

  openEditDialog(toDoView: ToDoView): void {
    const dialogRef = this.dialog.open(EditToDoViewDialog, {
      width: '80%',
      data: toDoView
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.save(toDoView);
      }
    });
  }

  openDeleteDialog(toDoView: ToDoView): void {
    
    const dialogRef = this.dialog.open(DeleteDialog, {
      data: toDoView
    });

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed: ' + result);
      if (result) {
        this.delete(toDoView);
      }
      
    });
  }

}
