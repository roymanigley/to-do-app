import { Component, OnInit, Inject } from '@angular/core';
import { File } from '../../../model/file';
import { FileService } from '../../../service/file-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DeleteDialog } from '../../../dialog/delete/delete-dialog';
import { MatSnackBar } from '@angular/material';
import { EditFileDialog } from '../../../dialog/edit/file/edit-file-dialog';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit {

  files: File[];
  displayedColumns: string[] = ['id', 'name', 'mimeType', 'buttons'];
  dataSource = null;

  constructor(private fileService: FileService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.ngFetchData();
  }

  ngFetchData() {

    this.fileService.findAll().subscribe(data => {
      this.files = data;
      this.dataSource = new MatTableDataSource(this.files);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(file: File) {
    this.fileService.delete(file).subscribe(
      (val) => {
        this.snackBar.open("Record deleted", null, {
          duration: 2000,
        });
      },
      response => {

        this.snackBar.open("Failed to deleted record", null, {
          duration: 5000,
        });
      },
      () => {
          this.ngFetchData();
      }
    );
  }

  save(file: File) {
    this.fileService.save(file).subscribe(
      (val) => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
      response => {
        this.snackBar.open("Failed to save record", null, {
          duration: 5000,
        });
      },
      () => {
          console.log("The SAVE observable is now completed.");
          this.ngFetchData();
      }
    );
  }

  downloadFile(file: File): void {
        
    this.fileService.downloadFile(file);
  }

  openCreateDialog(): void {
    this.openEditDialog(new File());
  }

  openEditDialog(file: File): void {
    const dialogRef = this.dialog.open(EditFileDialog, {
      width: '80%',
      data: file
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.save(file);
      }
    });
  }

  openDeleteDialog(file: File): void {
    
    const dialogRef = this.dialog.open(DeleteDialog, {
      data: file
    });

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed: ' + result);
      if (result) {
        this.delete(file);
      }
      
    });
  }

}
