import { Component, OnInit } from '@angular/core';
import { ToDo } from '../../../model/to-do';
import { ToDoService } from '../../../service/to-do-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { DeleteDialog } from '../../../dialog/delete/delete-dialog';
import { EditToDoDialog } from 'src/app/dialog/edit/to-do/edit-to-do-dialog';
import { MatSnackBar } from '@angular/material';
import { FileListComponent } from '../file-list/file-list.component';
import { State } from 'src/app/model/state';
import { RelatedFilesDialog } from 'src/app/dialog/files/related-files-dialog';
import { RelatedCommentsDialog } from 'src/app/dialog/comments/related-comments-dialog';
import { RelatedRequirementsDialog } from 'src/app/dialog/requirements/related-requirements-dialog';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css'],
  providers: [ToDoService, FileListComponent, MatDialog, MatSnackBar]
})
export class ToDoListComponent implements OnInit {

  toDos: ToDo[];
  displayedColumns: string[] = ['id', 'title', 'buttons'];
  dataSource = null;
  afterSubmit : Function;
  constructor(private toDoService: ToDoService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.ngFetchData();
  }

  ngFetchData() {

    this.toDoService.findAll().subscribe(data => {
      this.toDos = data;
      this.dataSource = new MatTableDataSource(this.toDos);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(toDo: ToDo) {
    this.toDoService.delete(toDo).subscribe(
      () => {
        this.snackBar.open("Record deleted", null, {
          duration: 2000,
        });
      },
      () => {

        this.snackBar.open("Failed to deleted record", null, {
          duration: 5000,
        });
      },
      () => {
          this.afterSubmit();
      }
    );
  }


  save(toDo: ToDo, afterSubmit?: Function) {
    afterSubmit = afterSubmit ? afterSubmit : () => this.afterSubmit();
    
    this.toDoService.save(toDo).subscribe(
      () => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
      () => {
        this.snackBar.open("Failed to save record", null, {
          duration: 5000,
        });
      },
      () => {
          console.log("The SAVE observable is now completed.");
          afterSubmit();
      }
    );
  }


  openCreateDialog(afterSubmit?: Function, availableStates?: State[]): void {
    
    this.openEditDialog(new ToDo(), afterSubmit, availableStates);
  }

  openEditDialog(toDo: ToDo, afterSubmit?: Function, availableStates?: State[]): void {
    this.afterSubmit = afterSubmit ? afterSubmit : () => this.ngFetchData();

    toDo.availableStates = availableStates;
    const dialogRef = this.dialog.open(EditToDoDialog, {
      width: '80%',
      data: toDo
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.save(toDo);
      }
    });
  }

  openDeleteDialog(toDo: ToDo, afterSubmit?: Function): void {
    this.afterSubmit = afterSubmit ? afterSubmit : () => this.ngFetchData();

    const dialogRef = this.dialog.open(DeleteDialog, {
      data: toDo
    });

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed: ' + result);
      if (result) {
        this.delete(toDo);
      }
      
    });
  }

  openRelatedFilesDialog(toDo: ToDo): void {

    const dialogRef = this.dialog.open(RelatedFilesDialog, {
      width: '80%',
      data: toDo
    });
  }

  openRelatedCommentsDialog(toDo: ToDo): void {

    const dialogRef = this.dialog.open(RelatedCommentsDialog, {
      width: '80%',
      data: toDo
    });
  }

  openRelatedRequirementsDialog(toDo: ToDo): void {

    const dialogRef = this.dialog.open(RelatedRequirementsDialog, {
      width: '80%',
      data: toDo
    });
  }
}
