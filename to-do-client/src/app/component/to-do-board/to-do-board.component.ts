import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { ToDoService } from 'src/app/service/to-do-service.service';
import { ToDo } from 'src/app/model/to-do';
import { State } from 'src/app/model/state';
import { StateService } from 'src/app/service/state-service.service';
import { MatSnackBar } from '@angular/material';
import { ToDoListComponent } from '../crud/to-do-list/to-do-list.component';
import { UserService } from 'src/app/service/user-service.service';
import { TagService } from 'src/app/service/tag-service.service';
import { User } from 'src/app/model/user';
import { Tag } from 'src/app/model/tag';
import { ANIMATION_MODULE_TYPE } from '@angular/platform-browser/animations';
import { ToDoViewService } from 'src/app/service/to-do-view-service-service.service';
import { ToDoView } from 'src/app/model/to-do-view';
import { ImageUtil } from 'src/app/util/ImageUtil';
import { AuthService } from 'src/app/service/auth-service.service';
@Component({
  selector: 'app-to-do-board',
  templateUrl: './to-do-board.component.html',
  styleUrls: ['./to-do-board.component.css'],
  providers: [ToDoListComponent,
  {provide: ANIMATION_MODULE_TYPE, useValue: 'BrowserAnimations'}
  ]
})
export class ToDoBoardComponent implements OnInit {

  groupedToDos: Map<State, ToDo[]> = new Map;
  selectedUsers: User[] = [];
  selectedTags: Tag[] = [];
  selectedStates: State[] = [];
  allUsers: User[] = [];
  allTags: Tag[] = [];
  allStates: State[] = [];
  showFilters: Boolean = false;
  loading: Boolean = false;
  showDetails: Boolean = false

  selectedToDo: ToDo;
  

  toDoViews: ToDoView[] = [];
  selectedView: ToDoView;

  constructor(private toDoService: ToDoService, private toDoViewService: ToDoViewService, private userService: UserService, private tagService: TagService, private stateService: StateService, private snackBar: MatSnackBar, private toDoListComponent: ToDoListComponent, private authService: AuthService) {
    
  }

  onViewSelect(toDoView: ToDoView): void {

    this.loading = true;
    this.toDoViewService.findByToDoView(toDoView).subscribe( fetchedView => {

      this.selectedView = fetchedView; 
      this.allStates = this.selectedView.states;
      this.selectedTags = this.selectedView.tags;
      
      this.selectedStates = this.selectedView.states;

      this.toDoService.findAllByStates(this.selectedView.states).subscribe((toDos) => {
        this.groupedToDos = new Map;
        this.allStates.sort((stateA, stateB) => {return stateA.sorter - stateB.sorter})
          .forEach(state => {
            let toDosByState = toDos.filter(t => t.state.id == state.id);
            this.groupedToDos.set(state, toDosByState);
        });

        
        this.snackBar.open("View loaded", null, {
          duration: 2000,
        });
        this.loading = false;
      });

    });
    
  }

  refresh(): void {
    this.init();
    this.onViewSelect(this.selectedView);
  }

  ngOnInit(): void {
    this.resetUserFilter();
    this.init();  
  }

  init(): void {
    this.loading = true;
    this.userService.findAll().subscribe((allUsers) => {
      this.allUsers = allUsers;
    });

    this.tagService.findAll().subscribe((allTags) => {
      this.allTags = allTags;
    });

    this.toDoViewService.findAll().subscribe(views => {
      this.toDoViews = views;
      this.loading = false;
    });
  }

  drop(event: CdkDragDrop<ToDo[]>, state: State) {
    
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {

      let toDo = event.previousContainer.data[event.previousIndex];
      toDo.state = state;
      this.toDoService.save(toDo).subscribe((response) => {
        this.snackBar.open("Record saved", null, {
          duration: 2000,
        });
      },
        (error) => {
          this.snackBar.open("Failed to save Record", null, {
            duration: 5000,
          });
        }
      );

      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  openEditDialog(toDo: ToDo): void {
    this.toDoListComponent.openEditDialog(toDo,() => {this.refresh()}, this.selectedView.states);
  }

  openDeleteDialog(toDo: ToDo): void {
    this.toDoListComponent.openDeleteDialog(toDo, () => {this.refresh()});
  }

  openCreateDialog(): void {
    this.toDoListComponent.openCreateDialog(() => {this.refresh()}, this.selectedView.states);
  }

  isStateVisible(state: State): Boolean {
    
    if (this.selectedStates.length == 0) {
      return true;
    }
    return this.selectedStates.find((selectedState) => selectedState.id == state.id) != null
  } 


  resetTagFilter(): void {
    this.selectedTags = [];
  }

  clearUserFilter(): void {
    this.selectedUsers = [];
  } 

  resetUserFilter(): void {
    this.selectedUsers = [this.authService.getCurrentUser()];
  }

  showHideFilters(): void {
    this.showFilters = !this.showFilters;
  }

  showHideDetails(): void {
    this.showDetails = !this.showDetails;
  }
  
  showHideToDoDetails(toDo: ToDo) {
    toDo.showDetails = !toDo.showDetails;
  }
  filterToDos(toDos: ToDo[]): ToDo[] {
    if (this.selectedUsers.length < 1 && this.selectedTags.length < 1) {
        return toDos;
    }
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return toDos.filter( toDo => { 
      let userFound = this.selectedUsers.length < 1;
      let tagFound = this.selectedTags.length < 1;
      
      for(let i = 0; i < toDo.users.length; i++) {
        for(let j = 0; j < this.selectedUsers.length; j++) {
          if (toDo.users[i].id == this.selectedUsers[j].id){
            userFound = true;
            break;
          }
        }
      }

      for(let i = 0; i < toDo.tags.length; i++) {
        for(let j = 0; j < this.selectedTags.length; j++) {
          if (toDo.tags[i].id == this.selectedTags[j].id){
            tagFound = true;
            break;
          }
        }
      }
      return userFound && tagFound;
    });
  }

  getUserImageURL(user: User): String {
    
    return ImageUtil.getUserImageURL(user);
  }

  userComparer(o1: User, o2: User): boolean {
    
    // if possible compare by object's name property - and not by reference.
    return o1 && o2 ? o1.id === o2.id : o2 === o2;
  }

  stateComparer(o1: State, o2: State): boolean {
    // if possible compare by object's name property - and not by reference.
    return o1 && o2 ? o1.id === o2.id : o2 === o2;
  }

  tagComparer(o1: Tag, o2: Tag): boolean {
    // if possible compare by object's name property - and not by reference.
    return o1 && o2 ? o1.id === o2.id : o2 === o2;
  }

  getCurrentUser(): User {
    return this.authService.getCurrentUser();
  }

  setSelectedToDo(toDo: ToDo): void {
    this.selectedToDo = toDo;
  }

  setExitState(toDo: ToDo): void {
    toDo.state = this.selectedView.exitState;
    this.toDoListComponent.save(toDo, () => this.refresh());
  }
  
  openRelatedFilesDialog(toDo: ToDo): void {

    this.toDoListComponent.openRelatedFilesDialog(toDo);
  }
  
  openRelatedCommentsDialog(toDo: ToDo): void {

    this.toDoListComponent.openRelatedCommentsDialog(toDo);
  }

  openRelatedRequirementsDialog(toDo: ToDo): void {

    this.toDoListComponent.openRelatedRequirementsDialog(toDo);
  }
}
