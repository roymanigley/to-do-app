import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from '../service/auth-service.service';

@Injectable()
export class JsessionHttpInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log("Intercepring Request");
    //if (this.auth.getJSessionId() != null) {
        // Clone the request to add the new header
        const clonedRequest = request.clone({ headers: request.headers.set('Set-Cookie', 'jsessionid=' + this.auth.getJSessionId()) });
        // Pass control to the next request
        return next.handle(clonedRequest);
    //} else {
        // Pass control to the next request
     //   return next.handle(request);
    //}
  }
}