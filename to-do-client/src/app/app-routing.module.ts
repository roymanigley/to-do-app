import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToDoListComponent } from './component/crud/to-do-list/to-do-list.component';
import { TagListComponent } from './component/crud/tag-list/tag-list.component';
import { StateListComponent } from './component/crud/state-list/state-list.component';
import { FileListComponent } from './component/crud/file-list/file-list.component';
import { ToDoBoardComponent } from './component/to-do-board/to-do-board.component';
import { UserListComponent } from './component/crud/user-list/user-list.component';
import { ToDoViewListComponent } from './component/crud/to-do-view-list/to-do-view-list.component';
import { LoginComponent } from './security/login/login.component';


const routes: Routes = [
  { path: 'to-do-list', component: ToDoListComponent },
  { path: 'tag-list', component: TagListComponent },
  { path: 'state-list', component: StateListComponent },
  { path: 'file-list', component: FileListComponent },
  { path: 'user-list', component: UserListComponent },
  { path: 'to-do-view-list', component: ToDoViewListComponent },
  { path: 'to-do-board', component: ToDoBoardComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
