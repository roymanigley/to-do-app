import { Component } from '@angular/core';
import { AuthService } from './service/auth-service.service';
import { User } from './model/user';
import { UserListComponent } from './component/crud/user-list/user-list.component';
import { MatSnackBar } from '@angular/material';
import { environment } from 'src/environments/environment';
import { ImageUtil } from './util/ImageUtil';
import { ImageService } from './service/image-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ UserListComponent ]
})
export class AppComponent {
  title = 'to-do-client';

  constructor(private authService: AuthService, private userListComponent: UserListComponent, private snackBar: MatSnackBar, private imageService: ImageService) {

  }

  isUserLoggedIn(): Boolean {

    return this.getCurrentUser() != null;
  }

  getCurrentUser(): User {
    return this.authService.getCurrentUser();
  }

  logout(): void {
    this.authService.logout().subscribe(() => {
      this.snackBar.open("Logout success", null, {
        duration: 2000,
      });
    })
  }

  openEditUserDialog(): void {

    this.userListComponent.openEditDialog(this.getCurrentUser());
  }

  openPasswordDialog(): void {

    this.userListComponent.openPasswordDialog(this.getCurrentUser());
  }

  getUserImageURL(user: User): string {
    
    return ImageUtil.getUserImageURL(user);
  }
}
