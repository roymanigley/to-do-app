import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth-service.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;

  constructor(private authService: AuthService, private snackBar: MatSnackBar, public router: Router) {

   }

  ngOnInit() {
  }

  login(): void {

    this.authService.login(this.username, this.password, (username) => {
      this.router.navigate(['to-do-board']);
      this.snackBar.open("Login success: " + this.username, null, {
        duration: 2000,
      });
    }, (username) => {
      this.snackBar.open("Login failed: " + this.username, null, {
        duration: 5000,
      });
    })
  }
}
