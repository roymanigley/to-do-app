export const environment = {
  production: true,
  restApplicationBaseUrl: "/api/",
  authApplicationLoginUrl: "/auth/"
};
