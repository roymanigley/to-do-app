package ch.bytecrowd.todo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SecurityFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityFilter.class);
    private static final Boolean DISABLE_SECURITY_FILTER = System.getenv("DISABLE_SECURITY_FILTER") != null && Boolean.valueOf(System.getenv("DISABLE_SECURITY_FILTER"));

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (((HttpServletRequest)request).getSession().getAttribute(SecurityConfig.SESSION_ATTR_KEY) != null || DISABLE_SECURITY_FILTER) {
            if (LOG.isDebugEnabled())
                LOG.info("SecurityFilter: OK");
            chain.doFilter(request, response);
        } else {
            if (LOG.isDebugEnabled())
               LOG.debug("SecurityFilter: NOK");
            ((HttpServletResponse)response).setStatus(401);
        }
    }
}
