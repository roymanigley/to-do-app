package ch.bytecrowd.todo.security;

import ch.bytecrowd.todo.model.User;
import ch.bytecrowd.todo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@SessionScope
public class SecurityController {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityController.class);

    private UserRepository userRepository;

    public SecurityController(@Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * @return map with jsessionid and user id
     */
    @GetMapping("/auth")
    @CrossOrigin
    public Map login(HttpServletRequest request, @RequestParam String username, @RequestParam String password) {

        if (userRepository.count() < 1) {
            final User user = new User();
            user.setUsername(username);
            user.setDisplayName(username);
            user.setPassword(password);
            userRepository.saveAndFlush(user);
        }

        final User currentUser = userRepository.findUserByUsernameAndPassword(username, password).orElseThrow(() -> new IllegalArgumentException("Login failed: " + username));
        final HashMap<String, Object> map = new HashMap();
        if (currentUser != null) {
            map.put("JSESSIONID", request.getSession().getId());
            map.put("USER_ID", currentUser.getId());
            request.getSession().setAttribute(SecurityConfig.SESSION_ATTR_KEY, currentUser);

            LOG.info("user is logged in: " + currentUser.getUsername());
        }
        return map;
    }

    @GetMapping("/auth/logout")
    @CrossOrigin
    public void logout(HttpServletRequest request) {
        request.getSession().setAttribute(SecurityConfig.SESSION_ATTR_KEY, null);
        request.getSession().invalidate();
    }
}
