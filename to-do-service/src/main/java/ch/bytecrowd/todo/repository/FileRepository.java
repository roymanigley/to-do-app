package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.File;
import ch.bytecrowd.todo.model.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FileRepository extends JpaRepository<File, Long> {

    public List<File> findFileByToDoId(Long idToDo);
}
