package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.ToDoComment;
import ch.bytecrowd.todo.model.ToDoView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ToDoCommentRepository extends JpaRepository<ToDoComment, Long> {

    @Query("SELECT DISTINCT c FROM ToDoComment c LEFT JOIN FETCH c.user u LEFT JOIN FETCH c.toDo t WHERE c.toDo.id = :idToDo ORDER BY c.time DESC")
    public List<ToDoComment> findAllByToDoIdOrderByTimeDesc(@Param("idToDo") Long idToDo);
}
