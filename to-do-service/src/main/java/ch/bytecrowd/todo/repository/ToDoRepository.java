package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDo, Long> {

    @Query("select distinct t from ToDo t JOIN FETCH t.state s LEFT JOIN FETCH t.tags tgs LEFT JOIN FETCH t.users usr LEFT JOIN FETCH t.files f where s.id in (:ids)")
    public List<ToDo> findAllByStateIdIsIn(@Param("ids") List<Long> states);
}
