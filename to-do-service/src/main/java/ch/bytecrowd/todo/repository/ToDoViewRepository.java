package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.ToDoView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToDoViewRepository extends JpaRepository<ToDoView, Long> {

}
