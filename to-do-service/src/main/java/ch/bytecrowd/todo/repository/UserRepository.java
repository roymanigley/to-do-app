package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.State;
import ch.bytecrowd.todo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    public Optional<User> findUserByUsernameAndPassword(String username, String password);
}
