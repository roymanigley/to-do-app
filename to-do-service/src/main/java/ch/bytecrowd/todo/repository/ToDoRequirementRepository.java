package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.ToDoComment;
import ch.bytecrowd.todo.model.ToDoRequirement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ToDoRequirementRepository extends JpaRepository<ToDoRequirement, Long> {

    @Query("SELECT DISTINCT rq FROM ToDoRequirement rq JOIN FETCH rq.toDo t WHERE rq.toDo.id = :idToDo")
    public List<ToDoRequirement> findAllByToDoIdOrderByTimeDesc(@Param("idToDo") Long idToDo);
}
