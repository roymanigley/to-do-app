package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.State;
import ch.bytecrowd.todo.model.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateRepository extends JpaRepository<State, Long> {

}
