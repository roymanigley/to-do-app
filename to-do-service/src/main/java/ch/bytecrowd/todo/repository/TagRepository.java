package ch.bytecrowd.todo.repository;

import ch.bytecrowd.todo.model.Tag;
import ch.bytecrowd.todo.model.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {

}
