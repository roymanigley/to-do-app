package ch.bytecrowd.todo.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "STATE")
public class State {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_STATE")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "SORTER")
    private Integer sorter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSorter() {
        return sorter;
    }

    public void setSorter(Integer sorter) {
        this.sorter = sorter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Objects.equals(id, state.id) &&
                Objects.equals(name, state.name) &&
                Objects.equals(sorter, state.sorter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sorter);
    }

    @Override
    public String toString() {
        return "State{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sorter=" + sorter +
                '}';
    }
}
