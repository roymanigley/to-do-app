package ch.bytecrowd.todo.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "TO_DO")
public class ToDo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TO_DO")
    private Long id;

    @Column(name = "TITLE", nullable = false, length = 255)
    private String title;

    @Lob
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Tag> tags = new LinkedHashSet<>();

    @ManyToMany
    private Set<User> users = new LinkedHashSet<>();

    @OneToMany(mappedBy = "toDo")
    private Set<File> files = new LinkedHashSet<>();

    @OneToMany(mappedBy = "toDo")
    private Set<ToDoRequirement> toDoRequirements = new LinkedHashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_STATE", nullable = false)
    private State  state;

    @Column(name = "DUE_DATE")
    private LocalDateTime dueDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Set<File> getFiles() {
        return files;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getCountToDoRequirementsAll() {

        return toDoRequirements == null ? 0 : toDoRequirements.size();
    }

    public Integer getCountToDoRequirementsDone() {

        if (toDoRequirements == null) {
            return 0;
        } else {
            final Long count = toDoRequirements.stream().filter(t -> t.getDone()).count();
            return count.intValue();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDo toDo = (ToDo) o;
        return Objects.equals(id, toDo.id) &&
                Objects.equals(title, toDo.title) &&
                Objects.equals(description, toDo.description) &&
                Objects.equals(state, toDo.state) &&
                Objects.equals(dueDate, toDo.dueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, state, dueDate);
    }

    @Override
    public String toString() {
        return "ToDo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", state=" + state +
                ", dueDate=" + dueDate +
                '}';
    }
}
