package ch.bytecrowd.todo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "FILE")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FILE")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DATA")
    @Lob
    @JsonIgnore
    private byte[] data;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ID_TO_DO")
    @JsonIgnore
    private ToDo toDo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public ToDo getToDo() {
        return toDo;
    }

    public void setToDo(ToDo toDo) {
        this.toDo = toDo;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return Objects.equals(id, file.id) &&
                Objects.equals(name, file.name) &&
                Arrays.equals(data, file.data) &&
                Objects.equals(mimeType, file.mimeType) &&
                Objects.equals(toDo, file.toDo);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name, mimeType, toDo);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", data=" + Arrays.toString(data) +
                ", mimeType='" + mimeType + '\'' +
                ", toDo=" + toDo +
                '}';
    }
}
