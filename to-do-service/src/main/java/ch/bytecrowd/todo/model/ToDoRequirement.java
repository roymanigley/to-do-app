package ch.bytecrowd.todo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name="TO_DO_REQUIREMENT")
public class ToDoRequirement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TO_DO_REQUIREMENT")
    private Long id;

    @Lob
    @Column(name = "TEXT", nullable = false)
    private String text;

    @Column(name = "DONE", nullable = false)
    private Boolean done = Boolean.FALSE;

    @ManyToOne
    @JoinColumn(name = "ID_TO_DO", nullable = false)
    private ToDo toDo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public ToDo getToDo() {
        return toDo;
    }

    public void setToDo(ToDo toDo) {
        this.toDo = toDo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDoRequirement that = (ToDoRequirement) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(text, that.text) &&
                Objects.equals(done, that.done) &&
                Objects.equals(toDo, that.toDo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, done, toDo);
    }

    @Override
    public String toString() {
        return "ToDoRequirement{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", done=" + done +
                ", toDo=" + toDo.getId() +
                '}';
    }
}
