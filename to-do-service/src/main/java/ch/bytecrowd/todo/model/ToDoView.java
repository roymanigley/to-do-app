package ch.bytecrowd.todo.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "TO_DO_VIEW")
public class ToDoView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TO_DO_VIEW")
    private Long id;

    @Column(name = "VIEW_NAME", nullable = false)
    private String viewName;

    @ManyToOne
    @JoinColumn(name = "ID_EXIT_STATE", nullable = false)
    private State exitState;

    @ManyToMany
    @OrderBy(value = "sorter")
    private Set<State> states;

    @ManyToMany
    private Set<Tag> tags;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public State getExitState() {
        return exitState;
    }

    public void setExitState(State exitState) {
        this.exitState = exitState;
    }

    public Set<State> getStates() {
        return states;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDoView toDoView = (ToDoView) o;
        return Objects.equals(id, toDoView.id) &&
                Objects.equals(viewName, toDoView.viewName) &&
                Objects.equals(exitState, toDoView.exitState);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, viewName, exitState);
    }

    @Override
    public String toString() {
        return "ToDoView{" +
                "id=" + id +
                ", viewName='" + viewName + '\'' +
                ", exitState=" + exitState +
                ", states=" + states +
                ", tags=" + tags +
                '}';
    }
}
