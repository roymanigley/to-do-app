package ch.bytecrowd.todo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name="TO_DO_COMMENT")
public class ToDoComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TO_DO_COMMENT")
    private Long id;

    @Lob
    @Column(name = "TEXT", nullable = false)
    private String text;

    @Column(name = "DATE", nullable = false)
    private LocalDateTime time;

    @ManyToOne
    @JoinColumn(name = "ID_TO_DO_USER", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "ID_TO_DO", nullable = false)
    private ToDo toDo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ToDo getToDo() {
        return toDo;
    }

    public void setToDo(ToDo toDo) {
        this.toDo = toDo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDoComment that = (ToDoComment) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(text, that.text) &&
                Objects.equals(time, that.time) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, time, user);
    }

    @Override
    public String toString() {
        return "ToDoComment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", time=" + time +
                ", user=" + user +
                ", toDo=" + toDo.getId() +
                '}';
    }
}
