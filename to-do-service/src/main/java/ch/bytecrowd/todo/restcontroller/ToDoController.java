package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.ToDo;
import ch.bytecrowd.todo.model.User;
import ch.bytecrowd.todo.repository.TagRepository;
import ch.bytecrowd.todo.repository.ToDoRepository;
import ch.bytecrowd.todo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/ToDo")
public class ToDoController {


    private static final Logger LOG = LoggerFactory.getLogger(ToDoController.class);

    private ToDoRepository repository;
    private TagRepository tagRepository;
    private UserRepository userRepository;

    public ToDoController(@Autowired ToDoRepository repository, @Autowired TagRepository tagRepository, @Autowired UserRepository userRepository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting ToDoRepository");
        this.repository = repository;
        this.tagRepository = tagRepository;
        this.userRepository = userRepository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<ToDo> getAllRecords() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from ToDoRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from ToDoRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(value =  "/State", method = RequestMethod.POST, produces = "application/json")
    public List<ToDo> findById(@RequestBody List<Long> sateIds) {

        if (LOG.isDebugEnabled())
            LOG.debug("Find toDoVs by state ids: " + Arrays.toString(sateIds.toArray()));

        return repository.findAllByStateIdIsIn(sateIds);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ToDo save(@RequestBody ToDo toDo) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save toDo: " + toDo);

        toDo.setTags(
                toDo.getTags().stream()
                        .map(tagRepository::save)
                        .collect(Collectors.toSet())
        );

        toDo.setUsers(
                toDo.getUsers().stream()
                        .map(u -> userRepository.findById(u.getId()).get())
                        .collect(Collectors.toSet())
        );

        try {
            return repository.save(toDo);
        } catch (Exception e) {
            LOG.error("Save ToDo failed: " + toDo, e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete toDo by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete toDo by id failed: " + id, e);
            throw e;
        }
    }
}
