package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.ToDoRequirement;
import ch.bytecrowd.todo.repository.ToDoRequirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/ToDoRequirement")
public class ToDoRequirementController {


    private static final Logger LOG = LoggerFactory.getLogger(ToDoRequirementController.class);

    private ToDoRequirementRepository repository;

    public ToDoRequirementController(@Autowired ToDoRequirementRepository repository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting ToDoRequirementRepository");
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<ToDoRequirement> getAll() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from ToDoRequirementRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from ToDoRequirementRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @GetMapping(value = "/ToDo/{idToDo}", produces = "application/json")
    public List<ToDoRequirement> getAllRequirementsByToDo(@PathVariable  Long idToDo) {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching by todo id records from ToDoRequirementRepository: " + idToDo);

        try {
            return repository.findAllByToDoIdOrderByTimeDesc(idToDo);
        } catch (Exception e) {
            LOG.error("Fetching all records from ToDoRequirementRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ToDoRequirement save(@RequestBody ToDoRequirement toDoRequirement) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save toDoRequirement: " + toDoRequirement);

        try {
            return repository.save(toDoRequirement);
        } catch (Exception e) {
            LOG.error("Save ToDoRequirement failed: " + toDoRequirement, e);
            throw e;
        }
    }

    /*
    * curl -X DELETE "http://localhost:8080/ToDoRequirement/4"
    * */
    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete toDoRequirement by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete toDoRequirement by id failed: " + id, e);
            throw e;
        }
    }
}
