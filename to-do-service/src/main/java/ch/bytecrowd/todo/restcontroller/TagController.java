package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.Tag;
import ch.bytecrowd.todo.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/Tag")
public class TagController {


    private static final Logger LOG = LoggerFactory.getLogger(TagController.class);

    private TagRepository repository;

    public TagController(@Autowired TagRepository repository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting TagRepository");
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<Tag> getAllRecords() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from TagRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from TagRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public Tag save(@RequestBody Tag tag) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save tag: " + tag);

        try {
            return repository.save(tag);
        } catch (Exception e) {
            LOG.error("Save Tag failed: " + tag, e);
            throw e;
        }
    }

    /*
    * curl -X DELETE "http://localhost:8080/Tag/4"
    * */
    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete tag by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete tag by id failed: " + id, e);
            throw e;
        }
    }
}
