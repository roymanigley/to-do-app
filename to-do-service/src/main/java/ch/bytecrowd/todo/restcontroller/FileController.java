package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.File;
import ch.bytecrowd.todo.repository.FileRepository;
import ch.bytecrowd.todo.repository.ToDoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/File")
public class FileController {


    private static final Logger LOG = LoggerFactory.getLogger(FileController.class);

    private FileRepository repository;
    private ToDoRepository toDoRepository;

    public FileController(@Autowired FileRepository repository, @Autowired ToDoRepository toDoRepository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting FileRepository");
        this.repository = repository;
        this.toDoRepository = toDoRepository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<File> getAllRecords() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from FileRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from FileRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @GetMapping(value =  "/{id}")
    public HttpEntity<byte[]> downloadFile(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Download file by id: " + id);

        try {
            final File file = repository.findById(id).orElseThrow(() -> new IllegalArgumentException("No file found for id: " + id));

            HttpHeaders header = new HttpHeaders();
            header.set("Content-Type", "application/octet-stream");//file.getMimeType());
            header.set("Content-Disposition", "inline; filename=\"" + file.getName() +"\"");
            header.setContentLength(file.getData().length);
            return new HttpEntity<>(file.getData(), header);

        } catch (Exception e) {
            LOG.error("Download file by id failed: " + id, e);
            throw e;
        }
    }

    @CrossOrigin
    @GetMapping(value="/ToDo/{idToDo}", produces = "application/json")
    public List<File> getAllToDoRelatedRecords(@PathVariable Long idToDo) {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching ToDo Related files from FileRepository - odToDo: " + idToDo);

        try {
            return repository.findFileByToDoId(idToDo);
        } catch (Exception e) {
            LOG.error("Fetching ToDo Related files from FileRepository failed - odToDo: " + idToDo, e);
            throw e;
        }
    }

    @CrossOrigin
    @PostMapping(value = "/ToDo/{idToDo}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public File handleFileUpload(@RequestParam("file") MultipartFile multipartFile, @PathVariable Long idToDo) throws IOException {

        if (LOG.isDebugEnabled())
            LOG.debug("handleFileUpload: " + multipartFile.getName());

        final File file = new File();
        file.setName(multipartFile.getOriginalFilename());
        file.setMimeType(multipartFile.getContentType());
        file.setData(multipartFile.getBytes());
        file.setToDo(toDoRepository.findById(idToDo).orElseThrow(() -> new RuntimeException("handleFileUpload: idToDo is invalid - idToDo: " + idToDo)));
        return repository.save(file);
    }

    /*
     * Only updating name and mimeType is allowed, data can not bee changed
     * */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public File update(@RequestBody File file) {

        if (LOG.isDebugEnabled())
            LOG.debug("Update file: " + file);

        try {
            final File fetchedFile = repository.findById(file.getId()).orElseThrow(() -> new IllegalArgumentException("Only updating name and mimeType is allowed, data can not bee changed"));
            fetchedFile.setName(file.getName());
            fetchedFile.setMimeType(file.getMimeType());

            return repository.save(fetchedFile);
        } catch (Exception e) {
            LOG.error("Save file failed: " + file, e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete file by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete file by id failed: " + id, e);
            throw e;
        }
    }
}
