package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.ToDoComment;
import ch.bytecrowd.todo.repository.ToDoCommentRepository;
import ch.bytecrowd.todo.repository.ToDoCommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/ToDoComment")
public class ToDoCommentController {


    private static final Logger LOG = LoggerFactory.getLogger(ToDoCommentController.class);

    private ToDoCommentRepository repository;

    public ToDoCommentController(@Autowired ToDoCommentRepository repository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting ToDoCommentRepository");
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<ToDoComment> getAll() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from ToDoCommentRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from ToDoCommentRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @GetMapping(value = "/ToDo/{idToDo}", produces = "application/json")
    public List<ToDoComment> getAllCommentsByToDo(@PathVariable  Long idToDo) {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching by todo id records from ToDoCommentRepository: " + idToDo);

        try {
            return repository.findAllByToDoIdOrderByTimeDesc(idToDo);
        } catch (Exception e) {
            LOG.error("Fetching all records from ToDoCommentRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ToDoComment save(@RequestBody ToDoComment toDoComment) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save toDoComment: " + toDoComment);

        try {
            toDoComment.setTime(LocalDateTime.now());
            return repository.save(toDoComment);
        } catch (Exception e) {
            LOG.error("Save ToDoComment failed: " + toDoComment, e);
            throw e;
        }
    }

    /*
    * curl -X DELETE "http://localhost:8080/ToDoComment/4"
    * */
    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete toDoComment by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete toDoComment by id failed: " + id, e);
            throw e;
        }
    }
}
