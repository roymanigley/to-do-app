package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.State;
import ch.bytecrowd.todo.repository.StateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/State")
public class StateController {


    private static final Logger LOG = LoggerFactory.getLogger(StateController.class);

    private StateRepository repository;

    public StateController(@Autowired StateRepository repository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting StateRepository");
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<State> getAllRecords() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from StateRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from StateRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public State save(@RequestBody State state) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save state: " + state);

        try {
            return repository.save(state);
        } catch (Exception e) {
            LOG.error("Save State failed: " + state, e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete state by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete state by id failed: " + id, e);
            throw e;
        }
    }
}
