package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.User;
import ch.bytecrowd.todo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/api/User")
public class UserController {


    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private UserRepository repository;

    public UserController(@Autowired UserRepository repository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting UserRepository");
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<User> getAllRecords() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from UserRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from UserRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @GetMapping(value="/{id}", produces = "application/json")
    public User findById(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("findById from UserRepository - id: " + id);

        try {
            return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("findById from UserRepository failed - id: " + id));
        } catch (Exception e) {
            LOG.error("Fetching all records from UserRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @GetMapping(value =  "/Image/{id}", produces = "application/json")
    public HttpEntity<byte[]> downloadImage(@PathVariable Long id) throws UnsupportedEncodingException {

        if (LOG.isDebugEnabled())
            LOG.debug("Download file by id: " + id);

        try {
            final User user = repository.findById(id).orElseThrow(() -> new IllegalArgumentException("No file found for id: " + id));

            HttpHeaders header = new HttpHeaders();
            header.set("Content-Type", user.getImageMimeType());
            header.set("Content-Disposition", "inline; filename=\"" + user.getDisplayName() +"\"");
            header.setContentLength(user.getImageBytes().length);
            return new HttpEntity<byte[]>(user.getImageBytes(), header);

        } catch (Exception e) {
            LOG.error("Download file by id failed: " + id, e);
            throw e;
        }
    }

    /*
    * curl -X POST "http://localhost:8080/ToDo/" --data '{"title":"delectus aut autem","description":"laboriosam mollitia et enim quasi adipisci quia provident illum"}' --header "Content-Type: application/json"
    * */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public User save(@RequestBody User user, @RequestParam String password) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save user: " + user);

        try {
            if (user.getId() != null) {
                final User fetchedUser = repository.findById(user.getId()).orElseThrow(() -> new IllegalArgumentException(""));
                user.setPassword(fetchedUser.getPassword());
                fetchedUser.setUsername(user.getUsername());
                fetchedUser.setDisplayName(user.getDisplayName());
                user = fetchedUser;
            } else {
                user.setPassword(password);
            }
            return repository.save(user);
        } catch (Exception e) {
            LOG.error("Save user failed: " + user, e);
            throw e;
        }
    }

    @CrossOrigin
    @PostMapping(value = "/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void updatePassword(@PathVariable Long id, @RequestBody String password) {

        final User user = repository.findById(id).orElseThrow(() -> new IllegalArgumentException("findById from UserRepository failed - id: " + id));
        user.setPassword(password);
        repository.save(user);
    }

    @CrossOrigin
    @PostMapping(value = "/Image/{idUser}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public User handleFileUpload(@RequestParam("file") MultipartFile multipartFile, @PathVariable Long idUser) throws IOException {

        if (LOG.isDebugEnabled())
            LOG.debug("handleFileUpload: " + multipartFile.getName());

        final User user = repository.findById(idUser).orElseThrow(() -> new RuntimeException("handleFileUpload: User is invalid - idUser: " + idUser));
        user.setImageMimeType(multipartFile.getContentType() != null ? multipartFile.getContentType() : "application/octet-stream" );
        user.setImageBytes(multipartFile.getBytes());
        return repository.saveAndFlush(user);
    }

    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete user by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete user by id failed: " + id, e);
            throw e;
        }
    }
}
