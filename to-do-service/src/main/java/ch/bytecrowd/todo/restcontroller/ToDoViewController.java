package ch.bytecrowd.todo.restcontroller;

import ch.bytecrowd.todo.model.ToDoView;
import ch.bytecrowd.todo.repository.ToDoViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ToDoView")
public class ToDoViewController {


    private static final Logger LOG = LoggerFactory.getLogger(ToDoViewController.class);

    private ToDoViewRepository repository;

    public ToDoViewController(@Autowired ToDoViewRepository repository) {

        if (LOG.isDebugEnabled())
            LOG.debug("Injecting ToDoViewRepository");
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping(value =  "/{id}", produces = "application/json")
    public ToDoView findById(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Find toDoView by id: " + id);

        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("findById: idToDoView is invalid - idToDoView: " + id));
    }

    @CrossOrigin
    @GetMapping(produces = "application/json")
    public List<ToDoView> getAllRecords() {

        if (LOG.isDebugEnabled())
            LOG.debug("Fetching all records from ToDoViewRepository");

        try {
            return repository.findAll();
        } catch (Exception e) {
            LOG.error("Fetching all records from ToDoViewRepository failed", e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ToDoView save(@RequestBody ToDoView toDoView) {

        if (LOG.isDebugEnabled())
            LOG.debug("Save toDoView: " + toDoView);

        try {
            return repository.save(toDoView);
        } catch (Exception e) {
            LOG.error("Save ToDoView failed: " + toDoView, e);
            throw e;
        }
    }

    @CrossOrigin
    @RequestMapping(value =  "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {

        if (LOG.isDebugEnabled())
            LOG.debug("Delete toDoView by id: " + id);

        try {
            repository.deleteById(id);
        } catch (Exception e) {
            LOG.error("Delete toDoView by id failed: " + id, e);
            throw e;
        }
    }
}
