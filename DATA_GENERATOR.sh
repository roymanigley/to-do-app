#!/bin/bash
export REST_API='https://to-do-board.herokuapp.com'
export USERNAME='admin'
export PASSWORD='admin'
export JSON=$(curl $REST_API'/auth/?username='$USERNAME'&password='$PASSWORD)
export JSESSION=$(echo $JSON | sed 's/^.\+JSESSIONID":"\(.\+\)"\}/\1/g')
export HEADERS=

echo $JSON
echo $JSESSION

###########################################################################################################################################
# Backlog =>  Assign:
#             Development => Developping => Documenting => Assign:                       => Versioned => Assign Installation                      
#                                                             Test => Testing => Assign: Development (Add TAG)
#             Installation => Installed (Test) => Installed (Prod) => Assign:
#                                                                     Ready To Bill => Billed, Paid
###########################################################################################################################################
# STATES
curl $REST_API'/api/State/' --data-binary '{"name":"Backlog","sorter":1000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Assigned (Development)","sorter":2000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Developing","sorter":3000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Documenting","sorter":4000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Assigned (Testing)","sorter":5000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Testing","sorter":6000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Versioned","sorter":7000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Assign (Installation)","sorter":8000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Installed (Test)","sorter":9000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Installed (Production)","sorter":10000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Ready to bill","sorter":11000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Billed","sorter":12000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/State/' --data-binary '{"name":"Paid","sorter":13000}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION


# TAGS
curl $REST_API'/api/Tag/' --data-binary '{"tagName":"Prio: 1"}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/Tag/' --data-binary '{"tagName":"Prio: 2"}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/Tag/' --data-binary '{"tagName":"Prio: 3"}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/Tag/' --data-binary '{"tagName":"Test NOK"}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/Tag/' --data-binary '{"tagName":"Test OK"}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION
curl $REST_API'/api/Tag/' --data-binary '{"tagName":"Frage"}' $HEADERS -H 'Content-Type: application/json' -H 'Set-Cookie: jsessionid='$JSESSION -H 'User-Agent: Mozilla/5.0' -H 'Referer: https://to-do-board.herokuapp.com/to-do-client/state-list' -H 'Cookie: JSESSIONID='$JSESSION

